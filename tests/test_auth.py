import json
import time

from httpx import AsyncClient
from sqlalchemy import select

from app.core.config import settings
from app.models import User
from tests.conftest import async_session_maker

superuser_token = ""
testuser1_id = 0
testuser1_token = ""
testuser2_id = 0
testuser2_token = ""


async def test_register_superuser(ac: AsyncClient):
    """Регистрация и авторизация суперпользователя."""
    response = await ac.post('/auth/register', json={
        "email": "superuser@example.com",
        "password": "string",
        "is_active": True,
        "is_superuser": False,
        "is_verified": False
    })
    assert response.status_code == 201

    async with async_session_maker() as session:
        superuser = await session.execute(select(User))
        superuser = superuser.scalars().first()
        superuser.is_superuser = True
        await session.commit()


    response = await ac.post(
        '/auth/jwt/login',
        headers={"Content-Type": "application/x-www-form-urlencoded"},
        data={
            "username": "superuser@example.com",
            "password": "string"
        })
    assert response.status_code == 200

    global superuser_token
    superuser_token = json.loads(response.text)['access_token']


async def test_register_testuser1(ac: AsyncClient):
    """Регистрация и авторизация тестового пользователя - testuser1."""
    response = await ac.post('/auth/register', json={
        "email": "testuser1@example.com",
        "password": "test1",
        "is_active": True,
        "is_superuser": False,
        "is_verified": False
    })
    global testuser1_id
    testuser1_id = json.loads(response.text)['id']
    assert response.status_code == 201

    response = await ac.post(
        '/auth/jwt/login',
        headers={"Content-Type": "application/x-www-form-urlencoded"},
        data={
            "username": "testuser1@example.com",
            "password": "test1"
        })

    global testuser1_token
    testuser1_token = json.loads(response.text)['access_token']


async def test_register_testuser2(ac: AsyncClient):
    """Регистрация и авторизация тестового пользователя - testuser2."""
    response = await ac.post('/auth/register', json={
        "email": "testuser2@example.com",
        "password": "test2",
        "is_active": True,
        "is_superuser": False,
        "is_verified": False
    })
    global testuser2_id
    testuser2_id = json.loads(response.text)['id']
    assert response.status_code == 201

    response = await ac.post(
        '/auth/jwt/login',
        headers={"Content-Type": "application/x-www-form-urlencoded"},
        data={
            "username": "testuser2@example.com",
            "password": "test2"
        })

    global testuser2_token
    testuser2_token = json.loads(response.text)['access_token']


async def test_salary_info_create(ac: AsyncClient):
    """Создание суперпользователем записей о зарплате для testuser1 и testuser2."""
    salary_info_for_testuser1 = {
        "current_salary": 50000,
        "salary_increase_date": '2023-10-15',
        "user_id": f"{testuser1_id}"
    }

    salary_info_for_testuser2 = {
        "current_salary": 100000,
        "salary_increase_date": "2023-12-31",
        "user_id": f"{testuser2_id}"
    }

    response = await ac.post(
        '/salary_info',
        headers={'Authorization': f'Bearer {superuser_token}'},
        json=salary_info_for_testuser1
    )

    assert {"current_salary":50000,"salary_increase_date":"2023-10-15"} == json.loads(response.text)

    response = await ac.post(
        '/salary_info',
        headers={'Authorization': f'Bearer {superuser_token}'},
        json=salary_info_for_testuser2
    )

    assert {"current_salary":100000,"salary_increase_date":"2023-12-31"} == json.loads(response.text)


async def test_testuser1_get_own_salary_info(ac: AsyncClient):
    """Получение testuser1 своей информации о зарплате (с использованием токена)."""
    response = await ac.get(
        '/salary_info',
        headers={'Authorization': f'Bearer {testuser1_token}'},
    )

    assert {"current_salary": 50000, "salary_increase_date": "2023-10-15"} == json.loads(response.text)


async def test_testuser2_get_own_salary_info(ac: AsyncClient):
    """Получение testuser2 своей информации о зарплате (с использованием токена)."""
    response = await ac.get(
        '/salary_info',
        headers={'Authorization': f'Bearer {testuser2_token}'},
    )

    assert {"current_salary":100000,"salary_increase_date":"2023-12-31"} == json.loads(response.text)


async def test_get_salary_info_without_token(ac: AsyncClient):
    """Получение информации о зарплате без токена"""
    response = await ac.get(
        '/salary_info',
    )

    assert response.status_code == 401


async def test_get_testuser1_salary_info_with_expired_token(ac: AsyncClient):
    """Получение testuser1 информации о своей зарплате с использованием токена с истекшим сроком."""
    settings.jwt_lifetime_seconds = 1
    response = await ac.post(
        '/auth/jwt/login',
        headers={"Content-Type": "application/x-www-form-urlencoded"},
        data={
            "username": "testuser1@example.com",
            "password": "test1"
        })

    global testuser1_token
    testuser1_token = json.loads(response.text)['access_token']

    response = await ac.get(
        '/salary_info',
        headers={'Authorization': f'Bearer {testuser1_token}'},
    )

    assert {"current_salary": 50000, "salary_increase_date": "2023-10-15"} == json.loads(response.text)

    time.sleep(2)

    response = await ac.get(
        '/salary_info',
        headers={'Authorization': f'Bearer {testuser1_token}'},
    )

    assert response.status_code == 401
