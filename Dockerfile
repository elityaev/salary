FROM python:3.10-slim

RUN mkdir /salary

COPY alembic/ /salary/alembic
COPY app/ /salary/app
COPY tests /salary/tests
COPY ["poetry.lock", "pyproject.toml", ".env", "alembic.ini", "/salary/"]

RUN pip install "poetry==1.3.2"

WORKDIR /salary

RUN poetry config virtualenvs.create false
RUN poetry install
RUN alembic upgrade head

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8000"]
