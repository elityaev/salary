from datetime import date

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.core.db import Base


class SalaryInfo(Base):
    current_salary: Mapped[int] = mapped_column(nullable=False)
    salary_increase_date: Mapped[date]
    user_id: Mapped[int] = mapped_column(ForeignKey('user.id'), unique=True)

    user: Mapped['User'] = relationship(back_populates='salary_info')
