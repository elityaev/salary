from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.ext.asyncio import AsyncSession

from app.api.validators import (
    check_salary_info_exists,
    check_valid_salary_increase_date,
    check_user_exists
)
from app.schemas.salary_info import SalaryInfoCreate, SalaryInfoDB
from app.crud.salary_info import (
    create_salary_info, get_current_user_salary_info
)
from app.core.db import get_async_session
from app.core.user import current_superuser, current_user
from app.models.user import User

router = APIRouter()


@router.post(
    '/salary_info',
    response_model=SalaryInfoDB,
    dependencies=[Depends(current_superuser)]
)
async def create_new_salary_info(
        salary_info: SalaryInfoCreate,
        session: AsyncSession = Depends(get_async_session),

):
    """Создать запись о текущей зарплате и дате ее повышения
    (только для супер-юзера)

    - **current_salary**: текущая зарплата
    - **salary_increase_date**: дата повышения
    - **user_id**: id пользователя
    """
    await check_user_exists(salary_info.user_id, session)
    await check_salary_info_exists(salary_info.user_id, session)
    await check_valid_salary_increase_date(salary_info.salary_increase_date)
    new_salary_info = await create_salary_info(salary_info, session)
    return new_salary_info


@router.get('/salary_info', response_model=SalaryInfoDB)
async def get_salary_info(
        session: AsyncSession = Depends(get_async_session),
        user: User = Depends(current_user)
):
    """Получить сведения о зарплате текущего пользователя.
    (требуется токен, получаемый при аутентификации)
    """
    salary_info = await get_current_user_salary_info(user, session)
    if not salary_info:
        raise HTTPException(
            status_code=422,
            detail='Информации по вашей зарплате в базе нет',
        )
    return salary_info
