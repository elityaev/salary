from datetime import date

from fastapi import HTTPException
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.models import User
from app.models.salary_info import SalaryInfo


async def check_user_exists(
        user_id: int,
        session: AsyncSession
) -> None:
    db_user_exists = await session.execute(
        select(User).where(User.id == user_id)
    )
    db_user_exists = db_user_exists.scalars().first()
    if not db_user_exists:
        raise HTTPException(
            status_code=422,
            detail='Сотрудник с таким id еще не зарегистрирован',
        )


async def check_salary_info_exists(
        user_id: int,
        session: AsyncSession
) -> None:
    db_salary_info_exists = await session.execute(
        select(SalaryInfo).where(SalaryInfo.user_id == user_id)
    )
    db_salary_info_exists = db_salary_info_exists.scalars().first()
    if db_salary_info_exists:
        raise HTTPException(
            status_code=422,
            detail='Информация для данного сотрудника уже внесена',
        )


async def check_valid_salary_increase_date(
        salary_increase_date: date,
) -> None:
    if salary_increase_date < date.today():
        raise HTTPException(
            status_code=422,
            detail='Дата повышения зарплаты не может '
                   'быть позже сегодняшнего дня',
        )
