from datetime import date
from typing import Optional

from pydantic import BaseModel


class SalaryInfoBase(BaseModel):
    current_salary: Optional[int]
    salary_increase_date: Optional[date]


class SalaryInfoCreate(SalaryInfoBase):
    current_salary: int
    salary_increase_date: date
    user_id: int


class SalaryInfoDB(SalaryInfoBase):
    pass

    class Config:
        orm_mode = True
