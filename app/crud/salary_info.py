from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.models.salary_info import SalaryInfo
from app.schemas.salary_info import SalaryInfoCreate
from app.models.user import User


async def create_salary_info(
        new_salary_info: SalaryInfoCreate,
        session: AsyncSession
) -> SalaryInfo:
    new_salary_info_data = new_salary_info.dict()
    db_salary_info = SalaryInfo(**new_salary_info_data)
    session.add(db_salary_info)
    await session.commit()
    await session.refresh(db_salary_info)
    return db_salary_info


async def get_current_user_salary_info(
        user: User,
        session: AsyncSession
):
    salary_info = await session.execute(
        select(SalaryInfo).where(SalaryInfo.user_id == user.id)
    )
    salary_info = salary_info.scalars().first()
    return salary_info
