# Salary

[[_TOC_]]

## Описание задачи.
>Реализуйте REST-сервис просмотра текущей зарплаты и даты следующего
повышения. Из-за того, что такие данные очень важны и критичны, каждый
сотрудник может видеть только свою сумму. Для обеспечения безопасности, вам
потребуется реализовать метод где по логину и паролю сотрудника будет выдан
секретный токен, который действует в течение определенного времени. Запрос
данных о зарплате должен выдаваться только при предъявлении валидного токена.
>
## Использованные технологии.

- [Python](https://docs.python.org/release/3.10.9/)
- [Poetry](https://python-poetry.org/docs/)
- [FastAPI](https://fastapi.tiangolo.com/)
- [FastAPI Users](https://fastapi-users.github.io/fastapi-users/10.2/configuration/overview/) 
- [SQLite](https://www.sqlite.org/docs.html) 
- [Sqlalchemy](https://docs.sqlalchemy.org/en/20/)
- [Alembic](https://alembic.sqlalchemy.org/en/latest/index.html)
- [Uvicorn](https://www.uvicorn.org/)
- [Pytest](https://docs.pytest.org/en/7.1.x/contents.html)
- [Pytest-Asyncio](https://pytest-asyncio.readthedocs.io/en/latest/index.html)
- [Httpx](https://www.python-httpx.org/quickstart/)

## Инструкция по запуску проекта 
### Запуск проекта локально

1. Клонировать репозиторий.
    - клонировать с SSH
    ```bash
    git clone git@gitlab.com:elityaev/salary.git
    ```
    - клонировать с HTTPS
    ```bash
    git clone https://gitlab.com/elityaev/salary.git
    ```
2. Cоздать и активировать виртуальное окружение, установить зависимости.

    - установить poetry, следуя [инструкции с официального сайта](https://python-poetry.org/docs/); 
    - настроить и создать виртуальное окружение в папке проекта, установить зависимости, активировать виртуальное окружение   
    ```bash
    cd salary
    poetry config virtualenvs.in-project true
    poetry install
    poetry shell

    ```
3. В корневой директории `salary/` cоздать .env файл по следующему шаблону:

    ```
    DATABASE_URL=sqlite+aiosqlite:///./salary.db 
    FIRST_SUPERUSER_EMAIL=admin@example.com 
    FIRST_SUPERUSER_PASSWORD=admin
    JWT_LIFETIME_SECONDS=3600 # время жизни токена автоизации
    SECRET='<your_secret_for_hashing_password>'

    ```
4. Применить миграции
    ```bash
    alembic upgrade head
    ```
5. Запустить проекта
    ```bash
    uvicorn app.main:app --reload
    ```
### Запуск сервиса в контейнере Docker
* **из Dockerfile**
1. Клонировать репозиторий.
    - клонировать с SSH
    ```bash
    git clone git@gitlab.com:elityaev/salary.git
    ```
    - клонировать с HTTPS
    ```bash
    git clone https://gitlab.com/elityaev/salary.git
    ```
2. Создать образ из Dockerfile?запустив команду из корневой директории проекта 
```bash
docker build -t salary . 
```
3. Запустить контейнер из образа 

```bash
docker run --name salary  -it -p 8000:8000 salary  

```
* **из образа с DockerHub**

```bash
docker run -p 8000:8000 elityaev/salary:v1.1  

```
Для автоматического запуска файл .env уже заполнен значениями по умолчанию.
Для дальнейшего использования рекомендуется эти значения поменять. 

## Функциональность проекта 

При первом запуске проекта автоматически создается суперпользователь (администратор), от имени которого после авторизации можно вносить информациюю о заработной плате сотрудников.

Взаимодействовать с сервисом можно через 
* Swagger, в котором задокументированы возможности API и примеры запросов, перейдя по адресу http://127.0.0.1:8000/docs
* [Postman](https://www.postman.com/) - сервис для создания, тестирования, документирования, публикации и обслуживания API

### Регистрация пользователя

POST http://127.0.0.1:8000/auth/register 

Пример запроса:
```json
{
    "email": "user@example.com",
    "password": "string",
    "is_active": true,
    "is_superuser": false,
    "is_verified": false
}
```
_При отправке запроса будут обработаны только поля email и password, остальные будут проигнорированы_

### Авторизация

 * **Swagger** 

 Нажмите на символ замка в строке любого эндпоинта или на кнопку Authorize в верхней части Swagger. 
Появится окно для ввода логина и пароля:

![alt text](doc_images/Screenshot_2023-06-10_100816.png)

Заполните поля **username** и **password**. В выпадающем списке Client credentials location оставьте значение Authorization header, остальные два поля оставьте пустыми; нажмите кнопку Authorize. Если данные были введены правильно, и таблица в БД существует — появится окно с подтверждением авторизации:
![alt text](doc_images/Screenshot_2023-06-10_101819.png)

Теперь при отправке запроса к эндпоинтам, отмеченным замком (в т.ч. эндпоинту информации о заработной плате), будет передаваться заголовок **Authorization** с токеном, например:

```python
url -X 'GET' \
  'http://127.0.0.1:8000/salari_Info' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiZmE1NTI4MzUtZmM0Ni00YmFlLThjYWEtZjQyNDI1ZTEyZjg2IiwiYXVkIjpbImZhc3RhcGktdXNlcnM6YXV0aCJdLCJleHAiOjE2NTEwNjk0NjF9._E1QglaWjmyf6n3XUn7GhQbT62MSCn_r9Ll2aeTjotE' 
```
По этому токену приложение определит, какой пользователь отправил запрос. 

* **Postman**

На адрес http://127.0.0.1:8000//auth/jwt/login необходимо отправить POST-запрос с полями формы **username** и **password**. При отправке запроса должен быть установлен тип содержимого «данные формы»: Content-Type: application/x-www-form-urlencoded.

![alt text](doc_images/Screenshot_2023-06-10_102538.png)

![alt text](doc_images/Screenshot_2023-06-10_102600.png)

В ответе на запрос будет содержаться персональный токен, который необходимо указать в заголовке** Autorization** запроса информации о заработной плате. 

![alt text](doc_images/Screenshot_2023-06-10_103614.png)

### Внесение и нформации о заработной плате сотрудника

Вносить информацию может только авторизированный суперпользователь. Для этого необходимо отправить запрос 

POST http://127.0.0.1:8000/salary_info

Пример тела запроса:
```json
{
    "current_salary": 50000,
    "salary_increase_date": "2023-12-12",
    "user_id": 2
}

```
### Получение пользователем информации о заработной плате 

Получить информацию о заработной плате может только авторизованный пользователь при предоставлении токена, полученного при аутентификации. Срок действия токена ограничен (по умолчанию срок - 1 час). По его истечению пользователю необходимо вновь пройти процедуру аутентификации. 
Эндпоинт для получения информации о заработной плате

GET http://127.0.0.1:8000/salary_info

Пример ответа:

Пример тела запроса:
```json
{
"current_salary": 50000,
"salary_increase_date": "2023-12-12"
}

```
## Тестирование

Запустить тестирование можно, выполнив корневой директории проекта команду

```bash
pytest -v

```
Для тестирования сервиса, запущенного в контейнере необходимо выполнить следующие команды: 

```bash

docker exec -it <CONTAINER ID OR NAME> bash
pytest -v

```

---

**Автор**

_Литяев Евгений_

